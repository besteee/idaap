<?php
namespace Certificate\Controller;
use Common\Controller\AdminbaseController;
class IndexadminController extends AdminbaseController{
	protected $homemb;
	public function _initialize(){
		parent::_initialize();
		$this->homemb=M("ecms_homemb");
	}
	function index(){
		$page=m_page(5,10);
		$this->assign('page',$page['page']);
		$this->assign('data',$page['data']);
		$this->display(":index");
	}
	function search(){
		$where=$_POST['key'].'="'.$_POST['word'].'"';
		$page=m_page(4,10,'ecms_homemb',$where);
		$this->assign('page',$page['page']);
		$this->assign('data',$page['data']);
		$this->display(":index");
	}
	function add(){
		$this->display(":add");
	}
	function addPost(){
		eval('$arr = '.$_POST['input']);
		$arr['newstime']=time();
		echo $this->homemb->add($arr);
	}
	function alter(){
		$this->data=$this->homemb->where('id='.$_GET['id'])->select();
		$this->display(":alter");
	}
	function alterPost(){
		eval('$arr = '.$_POST['input']);
		echo $this->homemb->save($arr);
	}
	function cdelete(){
		$this->homemb->where('id='.$_GET['id'])->delete();
		$this->index();
	}
}
