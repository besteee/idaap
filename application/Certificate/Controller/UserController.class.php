<?php
namespace Certificate\Controller;
use Common\Controller\HomeBaseController; 
class UserController extends HomeBaseController{
	function index(){
		$this->display("index");
	}
	function userDiy(){
		$this->display("userDiy");
	}
	function upload(){
		$uploadname=m_upload();
		$this->assign('uploadname',$uploadname);
		$this->display("userDiy");
	}
}
